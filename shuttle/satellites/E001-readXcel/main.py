# --------------------------------------------------- COMMON LIBS 
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)
#scriptName = os.path.basename(__file__)[:-3]

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.reloader  import Reloader
from lib.helper_01  import Helper
from lib.router  import Router
from lib.exporter  import Exporter
#......................

router = Router( )
ROUTES =  router.getRoutes()
reloader = Reloader( currentFullRoute, ROUTES  )
helper = Helper( )
exporter = Exporter( )

sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import csv
import pandas as pd

# ---------------------------------------------------------------------- MAIN

if __name__ == '__main__':

    params = reloader.parseArgs( sys.argv[1:] )

    inputs = reloader.getInputs()
    inputFile =  inputs[0]["path"][0]
    
    xl  = pd.ExcelFile( inputFile, encoding="utf-8" )
    sheets = xl.sheet_names 
    #RES: [u'PRODUCTS', u'ZONES', u'INSTAGRAM', u'FACEBOOK', u'YOUTUBE', u'TWITTER']

    for selectedSheet in sheets:
            
        #selectedSheet = sheets[5]
        outputParams = reloader.getOutputPath( selectedSheet ) #PRODUCTS,..

        df = xl.parse( selectedSheet )
        #IGN: df = pd.read_excel(xl, selectedSheet)
        df.to_csv( outputParams["path"] + '.csv', index=False, encoding="utf-8" ) 


    reloader.saveMetas(); 
       
    