 # --------------------------------------------------- COMMON LIBS
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd())
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )

#Extend the Reporter with the desired Report-Class
from lib.reporters.MultiLayerTimeSerie import MultiLayerTimeSerie
multilayer = MultiLayerTimeSerie()
reporter.builder = multilayer.builder

#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv

import re
from cStringIO import StringIO

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )

if __name__ == '__main__':
  #Load
  inputs = reloader.getInputs()

  censoFile =  inputs[0]["path"][0]
  # dfCenso = exporter.csvToDF( censoFile )

  listRows = []

  row = {
    "age":0,
    "location":"",
    "gender":"",
    "population":0
  }

  #listLines  = open(censoFile).readlines()
  listLines = [line.rstrip('\n') for line in open(censoFile)]

  for line in listLines:
    if line.strip() is not "":
      nTabs = line.count('  ')
      
      if nTabs == 1:
        m = re.search('([0-9]+).+', line)
        if m:
          row["age"] = m.group(1)
      elif nTabs == 2:
        m = re.search('[0-9]+ (\w+)[\/;]+', line)
        if m:
          row["location"] =  m.group(1)
      else:
        m = re.search( '(\w+);([0-9]+).' , line)
        if m:
          row["gender"] = m.group(1)
          row["population"] = m.group(2)
          listRows.append( dict(row) )


  #print listRows    

  censoDf = pd.DataFrame( listRows )

  outputParams =  reloader.getOutputPath( "" )
  exporter.dfToCsv( censoDf, outputParams["path"] )
  metaResult = reloader.saveMetas()
