
import numpy as np

class itemRate( object ):
  def __init__(self, intRate ,strName ):
    self.rate = intRate
    self.name = strName


class itemGroup( object ):
  def __init__(self):
    self.data = []
    self.attr = {}

#float(rowData.iloc[0][ dataCols[0] ]),

class MultipleGroupsRate( object ):

    def __init__( self ):
      self.name = "MultipleGroupsRate"

    def builder( self, pSourceDf, blockKey, pDivider, pRateOfFields, pAggFields ):

        totals = {}
        for rfId, rateFieldKey in enumerate( pRateOfFields ):
            totals[ rateFieldKey ] = pSourceDf[ rateFieldKey ].sum()

        buildItem = {}
        uniquesA = pSourceDf[ pDivider[0] ].unique()
        uniquesB = pSourceDf[ pDivider[1] ].unique()

        for aId, aIt in enumerate( uniquesA ):
            buildItem[ aIt ] = {}
            for bId, bIt in enumerate( uniquesB ):
                subItem = itemGroup()
                buildItem[ aIt ][ bIt ] = {}

        for index, row in pSourceDf.iterrows():
            rowItem = itemGroup()
            for rfId, rateFieldKey in enumerate( pRateOfFields ):
              calculatedRate = round( ( row[ rateFieldKey ] * 100) / float( totals[ rateFieldKey ] ), 2 )
              rowItem.data.append( vars( itemRate( str(calculatedRate), rateFieldKey ) ) )

            for aggId, aggKey in enumerate( pAggFields ):
                rowItem.attr[ aggKey ] = str(row[ aggKey ])

            buildItem[  row[ pDivider[0] ] ][  row[ pDivider[1] ] ] = vars( rowItem )

        return buildItem
