
class mDate( object ):
    d = 0
    m = 0
    y = 0

class mLayer( object ):
    name = ""
    data = []
    color = 0



class MultiLayerTimeSerie( object ):

    def __init__( self ):
        self.name = "MultiLayerTimeSerie"
    
    def builder( self, pDf, yCol, layersCol, dataCols, pDimension ):
        #Build unique days list
        sDf = pDf.sort_values(by=[ yCol ])
        uniqueYCol =  pDf[ yCol ].unique()
          
        uniqueLayers = pDf[ layersCol ].unique()
        layers = []
        for idx, uniqColItem in enumerate( uniqueLayers ):

            #print idx, uniqColItem
            bLayer = mLayer()

            bLayer.name = uniqColItem
            bLayer.color = idx

            redDf = sDf[ sDf[ layersCol ] == uniqColItem ]
            bLayer.data = self.getLayerData( uniqColItem, redDf, uniqueYCol, yCol, dataCols )

            layers.append( vars( bLayer ) )

        return {
            "dimension": pDimension,
            "y":{
                "name":yCol,
                "data": list( uniqueYCol)
            },
            "layers": layers ,
            "config":{
                "opacity":True,
                "line":True,
                "dots":True,
                "grid":True,
                "dot_label":True
            },
            "metadata":{
                "y":{
                    "data":{
                        "type":"date",
                        "format":"YYYY-MM-DD"
                    }
                },
                "layers":{
                    "data":[
                        {
                           "name": dataCols[0],
                           "type":"float",
                           "description":"" 
                        },
                        {
                           "name": dataCols[1],
                           "type":"float",
                           "description":"" 
                        },
                        {
                           "name": dataCols[2],
                           "type":"float",
                           "description":"%" 
                        }
                    ]
                }

            }
        }
    
    def getLayerData( self, uniqColItem, cDf, pUniqueYCol, pYcol, dataCols ):
        # Barcelona, subData with "Barcelona", unique Dates, 'Date'
        #force_non_zero = True
        layerData = []
        for idd, datum in enumerate( pUniqueYCol ):
            singleData = []
            if datum in cDf[ pYcol ].values:
                rowData = cDf[ cDf[ pYcol ] == datum  ]
                # print rowData[ dataCols[0] ].values[0]
                # print rowData.iloc[0][ dataCols[0] ]
                singleData = [
                    float(rowData.iloc[0][ dataCols[0] ]),
                    float(rowData.iloc[0][ dataCols[1] ]),
                    float(rowData.iloc[0][ dataCols[2] ])
                ]
            else:
                singleData = [0,0,0]

            layerData.append( singleData )
        return layerData