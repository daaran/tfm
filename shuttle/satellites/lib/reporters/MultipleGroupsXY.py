
import numpy as np

class Frame( object ):
  def __init__(self):
    self.x = 0
    self.y = 0

class Serie( object ):
  def __init__(self):
    self.attr = {}
    self.data = []

#float(rowData.iloc[0][ dataCols[0] ]),

class MultipleGroupsXY( object ):

    def __init__( self ):
      self.name = "MultipleGroupsXY"
    
    def builder( self, pSourceDf, pGroups, blockKey ):
     
      builderData =  {}
     
      blockItem = pSourceDf #single element  

      #Barcelona

      blockSerie = Serie()
      blockUniqueName  = blockItem.iloc[0][ blockKey ]
      blockSerie.attr[ blockKey ] = blockUniqueName
      blockSerie.attr[ "type" ] = blockKey

      for g in pGroups:
        groupSerie = Serie();
        groupSerie.attr[ "name" ] = g["name"]
        groupSerie.attr[ "type" ] = g["type"]
        groupSerie.attr[ "label_x" ] = g["label_x"]
        groupSerie.attr[ "label_y" ] = g["label_y"]

        #Para cada grupo obtenemos la x y la y, y calculamos:
        # formula de la regresion lineal
        # hayamos el punto minimo y maximo segun la formula

        xList = pSourceDf[ g["x"] ].tolist()
        yList = pSourceDf[ g["y"] ].tolist()
        fit = np.polyfit( xList, yList, 1 )
        fit_fn = np.poly1d(fit) 
    
        groupSerie.attr[ "regression" ] = {
          "from": {
            "x":min(xList),
            "y":int( fit_fn( min(xList) ) )
          },
          "to": {
            "x":max(xList),
            "y":int( fit_fn( max(xList) ) )
          }
        }
       
        for index, row in blockItem.iterrows():
          rowData = Frame()
          rowData.x = row[  g["x"] ]
          rowData.y = row[  g["y"] ]
          groupSerie.data.append( vars( rowData ) )
      
        blockSerie.data.append( vars( groupSerie ) )

      return vars( blockSerie )


