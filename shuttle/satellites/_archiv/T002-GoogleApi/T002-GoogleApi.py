# -*- coding: utf-8 -*-
from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = ""
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

def youtube_search( pQ='Google', pMax_results=50 ):
  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)

  # Call the search.list method to retrieve results matching the specified
  # query term.
  #https://developers.google.com/resources/api-libraries/documentation/youtube/v3/python/latest/youtube_v3.search.html
  #pageToken='CDIQAA',
  search_response = youtube.search().list(
    q= pQ,
    part="id,snippet",
    maxResults=pMax_results,
   
    location="40.6346559,-4.4489562,8.25"
  ).execute()

  videos = []
  channels = []
  playlists = []
  print "###########"
  print search_response.get("nextPageToken")
  # Add each result to the appropriate list, and then display the lists of
  # matching videos, channels, and playlists.
  for search_result in search_response.get("items", []):
    if search_result["id"]["kind"] == "youtube#video":
      videos.append("%s (%s)" % (search_result["snippet"]["title"],
                                 search_result["id"]["videoId"]))
    elif search_result["id"]["kind"] == "youtube#channel":
      channels.append("%s (%s)" % (search_result["snippet"]["title"],
                                   search_result["id"]["channelId"]))
    elif search_result["id"]["kind"] == "youtube#playlist":
      playlists.append("%s (%s)" % (search_result["snippet"]["title"],
                                    search_result["id"]["playlistId"]))

  #.encode('utf8')  
  print videos
#   print "Videos:\n", "\n".join(videos), "\n"
#   print "Channels:\n", "\n".join(channels), "\n"
#   print "Playlists:\n", "\n".join(playlists), "\n"


if __name__ == "__main__":
  
  argQ = "Mobil"
  argMax = 50

  try:
    youtube_search( pQ=argQ, pMax_results=argMax )
  except HttpError, e:
    print "An HTTP error %d occurred:\n%s" % (e.resp.status, e.content)