 # --------------------------------------------------- COMMON LIBS 
import sys
reload(sys) 
sys.setdefaultencoding('utf8')
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )


#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS
from sklearn import preprocessing
import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )


if __name__ == '__main__':
    
    #Load
    inputs = reloader.getInputs()

    statsRaw =  inputs[0]["path"][0]
    statsDf = exporter.csvToDF( statsRaw )

    #print statsDf

    cPFB = statsDf['zipCode'].corr( statsDf['prints_facebook'], method='kendall'  )
    cHFB = statsDf['zipCode'].corr( statsDf['hits_facebook'] , method='kendall')
    
    cPIN = statsDf['zipCode'].corr( statsDf['prints_instagram'], method='kendall' )
    cHIN = statsDf['zipCode'].corr( statsDf['hits_instagram'], method='kendall' )

    cPTW = statsDf['zipCode'].corr( statsDf['prints_twitter'], method='kendall' )
    cHTW = statsDf['zipCode'].corr( statsDf['hits_twitter'], method='kendall' )

    cPYT = statsDf['zipCode'].corr( statsDf['prints_youtube'], method='kendall' )
    cHYT = statsDf['zipCode'].corr( statsDf['hits_youtube'], method='kendall' )


    d = { 
        'prints_facebook': [ cPFB ],
        'hits_facebook': [ cHFB ],
        'prints_instagram': [ cPIN ],
        'hits_instagram': [ cHIN ],
        'prints_twitter': [ cPTW ],
        'hits_twitter': [ cHTW ],
        'prints_youtube': [ cPYT ],
        'hits_youtube': [ cHYT ]
    
    }    
    dfCorrs = pd.DataFrame(data=d)
    dfCorrs = dfCorrs.T
    dfCorrs.columns = ["xRegion"]


    #print statsDf.corr( )

    outputParams =  reloader.getOutputPath( "coorRegionPlatform" )
    exporter.dfToCsv( dfCorrs, outputParams["path"] )
    metaResult = reloader.saveMetas() 
