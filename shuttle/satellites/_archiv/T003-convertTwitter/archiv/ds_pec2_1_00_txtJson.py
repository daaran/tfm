import json
import os
from os import listdir
from os.path import isfile, join

CURRENT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
DATA_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__),"./data_test/"))

'''
https://stackoverflow.com/questions/12451431/loading-and-parsing-a-json-file-with-multiple-json-objects-in-python

'''


inputDir  = DATA_DIR + "/"
#inputFile = 'out_1_data_2017.Apr.23.txt'



outputDir  = CURRENT_DIR + "/data_test/"
#outputFile = 'mod_' + inputFile

inputFilesList = [ f for f in listdir( inputDir ) if isfile( join( inputDir, f ) ) ]

for iFile in inputFilesList:
    data = []

    with open( os.path.join(inputDir, iFile ) ) as f:
        for line in f:
            data.append(json.loads(line))
            #print line

    outputFile = 'os00__F-' + iFile
    modName = outputFile.replace(".txt", ".json")

    with open( os.path.join( outputDir , modName ) , 'w') as outfile:
        json.dump( data , outfile , indent=4)
