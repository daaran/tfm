import sys
import os

from os import listdir
from os.path import isfile, join

import datetime as dt
import pandas as pd
from pandas.compat import StringIO
import numpy as np
import matplotlib.pyplot as plt
import json
from pandas.io.json import json_normalize
import dateutil



inputDir = "./data_test/"

inputFiles = [
    'os02__F-os01_3_B__F-os00__F-is00__out_1_data_2017.May.30.json__.json',
    'os02__F-os01_3_B__F-os00__F-is00__out_1_data_2017.May.30.json__1.json'
]

outputFile = 'os02_merged__F-os01_3_B__F-os00__F-is00__out_1_data_2017.May.30.json__.json'

n = []

for iFile in inputFiles:
    with open( os.path.join( inputDir , iFile ) , 'r') as infile:
        data  = json.load(infile)
        n += data
        print len(n)

with open( os.path.join( inputDir , outputFile ) , 'w') as outfile:
    json.dump( n , outfile , indent=2 , ensure_ascii=False)
