 # --------------------------------------------------- COMMON LIBS
import sys
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd())
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )

#Extend the Reporter with the desired Report-Class
from lib.reporters.MultiLayerTimeSerie import MultiLayerTimeSerie
multilayer = MultiLayerTimeSerie()
reporter.builder = multilayer.builder

#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS

import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv
import re

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )


if __name__ == '__main__':

    #Load
    inputs = reloader.getInputs()

    #Input Platfomrs files:

    platformId = params["platformId"];
    socialSource =  inputs[0]["path"][ platformId ]
    platformMatch = re.search('.*_.+[0-9]{3}_(.*).csv', socialSource )
    if not platformMatch is None:
        platformName = platformMatch.group(1)
        print platformName
    else:
        raise ValueError('Not Platform name match')


    dfSocial = exporter.csvToDF( socialSource )

    def calculateAge( x ):
        return int( np.mean(  [int(i) for i in x.split("-") ]  )  )

    #Process
    pListFilter = [ 'Age' ]

    pDf = dfSocial
    mP = pDf.groupby( pListFilter )['Prints'].sum().round( decimals= 2)
    mH = pDf.groupby( pListFilter )['Hits'].sum().round( decimals= 2)
    rDf = pd.concat([ mP , mH], axis=1, keys=['Prints', 'Hits'])
    rDf.reset_index( inplace=True)

    rDf['ctr'] = rDf.apply(lambda row: social.divisionCtr( row['Prints'],row['Hits']), axis=1 )
    rDf['meanAge'] = rDf['Age'].apply( lambda x:  calculateAge(x)  )

    #rDf[['year', 'month', 'day']]= rDf["Date"].str.split("-",  expand = True)

    outputParams =  reloader.getOutputPath( platformName )
    exporter.dfToCsv( rDf, outputParams["path"] )
    metaResult = reloader.saveMetas()
