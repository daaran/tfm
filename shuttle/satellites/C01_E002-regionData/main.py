 # --------------------------------------------------- COMMON LIBS 
import sys
reload(sys) 
sys.setdefaultencoding('utf8')
import os
from os import listdir
from os.path import isfile, join

currentFullRoute = os.path.abspath(os.path.join(os.path.dirname(__file__),"./"))
currentDir = os.path.basename(os.getcwd()) 
currentFileName = os.path.basename(__file__)

libDir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../"))
sys.path.append( libDir )

from lib.router import Router
router = Router( )
ROUTES =  router.getRoutes()

from lib.reloader import Reloader
reloader = Reloader( currentFullRoute, ROUTES  )

from lib.helper_01 import Helper
helper = Helper( )

from lib.exporter import Exporter
exporter = Exporter( )

 # Optional module 1
from lib.social import Social
social = Social( )


from lib.reporter import Reporter
reporter = Reporter(  ROUTES  )


#......................
sys.path.append( currentFullRoute )
#os.chdir( os.path.dirname(__file__) )

# --------------------------------------------------- CUSTOM LIBS
from sklearn import preprocessing
import datetime as dt
from datetime import date
import pandas as pd
import numpy as np
import math
import json
from pandas.io.json import json_normalize
import dateutil
import csv

# ---------------------------------------------------------------------- MAIN
params = reloader.parseArgs( sys.argv[1:] )


if __name__ == '__main__':
    
    #Load
    inputs = reloader.getInputs()

    postalCodes =  inputs[1]["path"][0]
    dfPostalCodes = exporter.csvToDF( postalCodes )

    uniqueRegions = dfPostalCodes[ "City" ].unique()
    uniqueZipcodes = dfPostalCodes[ "ZipCode" ].unique()
 


    def extractRegionStats( cityName ):

        socialSource0 =  inputs[0]["path"][ 0 ]
        socialSource1 =  inputs[0]["path"][ 1 ]
        socialSource2 =  inputs[0]["path"][ 2 ]
        socialSource3 =  inputs[0]["path"][ 3 ]

        dfFB = exporter.csvToDF( socialSource0 )
        dfINS = exporter.csvToDF( socialSource1 )
        dfTW = exporter.csvToDF( socialSource2 )
        dfYT = exporter.csvToDF( socialSource3 )

        #Process
        dfFB.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
        dfFB["City"] = social.addColumn_CitynameByZipcode( dfFB, dfPostalCodes )
        dfFB["Platform"] = "Facebook"
        dfFBred = dfFB[ dfFB["City"] == cityName ]

        outputParams =  reloader.getOutputPath( cityName )
        exporter.dfToCsv( dfFBred, outputParams["path"] )

        dfINS.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
        dfINS["City"] = social.addColumn_CitynameByZipcode( dfINS, dfPostalCodes )
        dfINS["Platform"] = "Instagram"
        dfINSred = dfINS[ dfINS["City"] == cityName ]

        dfTW.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
        dfTW["City"] = social.addColumn_CitynameByZipcode( dfTW, dfPostalCodes )
        dfTW["Platform"] = "Twitter"
        dfTWred = dfTW[ dfTW["City"] == cityName ]

        dfYT.rename(columns={'Zip Code': 'zipCode'}, inplace=True)
        dfYT["City"] = social.addColumn_CitynameByZipcode( dfYT, dfPostalCodes )
        dfYT["Platform"] = "Youtube"
        dfYTred = dfYT[ dfYT["City"] == cityName ]

        cityAllPlatformsDF = [dfFBred, dfINSred, dfTWred, dfYTred]
        dfCityAll =  pd.concat(cityAllPlatformsDF)

        DFfiltered = dfCityAll[["Date","Prints","Hits", "Platform" ]].copy()
        dfG = DFfiltered.groupby(['Date','Platform']).sum().reset_index()

        dfGS = dfG.pivot( index='Date', columns='Platform').reset_index()
        #print dfGS.columns.values
        
        #dfGS.columns =  dfGS.columns.droplevel()
        #dfGS.columns =  dfGS.columns.get_level_values(0)
        #dfGS.columns =  [' '.join(col).strip() for col in dfGS.columns.values]
        dfGS.columns =  [(lambda x: str(x[0] + "_" + x[1]).lower() )(col) for col in dfGS.columns.values]

        dfGS[['year', 'month', 'day']]= dfGS["date_"].str.split("-",  expand = True)
        dfGS["City"] = cityName
        return dfGS

    



    #listDFs = []

    for idx, reg in enumerate(uniqueRegions):
        print reg
        cityName = uniqueRegions[ idx ]   
        dfRegion = extractRegionStats( cityName );
        outputParams =  reloader.getOutputPath( cityName )
        exporter.dfToCsv( dfRegion, outputParams["path"] )
        #listDFs.append( dfRegion )


    # dfFinal = pd.concat( listDFs ).reset_index() 
    # outputParams =  reloader.getOutputPath( cityName )
    # exporter.dfToCsv( dfFinal, outputParams["path"] )
    
    metaResult = reloader.saveMetas() 